# React Hello World!

## Tổng có 4 nhánh Prelive, Live , DeployECR, Terraform

### 1. Success if the database is not empty
- Trong pipeline sẽ tạo ra file .env và thêm các biến môi trường vào file.Define biến *URL_DATABASE* chứa nội dung endpoint của database.Check nếu biến không được define thì pipeline sẽ fail.
--> Khi run pipeline `phải` cung cấp 1 giá trị nào đó cho biến *URL_DATABASE*.![image](./add_variable.png)

### 2. when the code is deployed, sending an email to notify related team
- Sử dụng luôn chức năng integrations của gitlab. Nó sẽ thông báo về các mail được define khi pipeline run success or fail . ![image](./integrations_mail.png)
- Ở đây nhánh default là nhánh Live .

### 3. Modiffy your CI/CD to deploy the Docker to AWS ECS (free-tier)
- Nhánh sử dụng là DeployECR

### 4. IAC Infrastructure as code
- Code terraform cho vpc và ec2 nằm ở nhánh Terraform

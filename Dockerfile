FROM node:14.16.0-alpine

WORKDIR /app

COPY . ./

RUN npm install

CMD ["npm", "start"]

EXPOSE 3000
